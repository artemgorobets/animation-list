package com.customertimes.animationlistactivity;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.customertimes.animationlistactivity.adapters.StickyArrAdapter;
import com.customertimes.animationlistactivity.models.ArrayGenerator;
import com.customertimes.animationlistactivity.models.ListArrModel;
import com.nhaarman.listviewanimations.appearance.StickyListHeadersAdapterDecorator;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class StickyHeaders extends BaseActivity {
    StickyListHeadersListView listView;
    StickyArrAdapter stickyArrAdapter;
    StickyListHeadersAdapterDecorator stickyListHeadersAdapterDecorator;
    ScaleInAnimationAdapter scaleInAnimationAdapter;
    private int mScrollState;
    ArrayList<ListArrModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticky_headers);
        ActionBar actionBar = getActionBar();

        actionBar.hide();
        BusProvider.getInstance().register(this);

        ArrayGenerator arrayGenerator = new ArrayGenerator();
        list = (ArrayList<ListArrModel>) arrayGenerator.getArray();

        listView = (StickyListHeadersListView) findViewById(R.id.sticky_listview);
        listView.setFitsSystemWindows(true);
        listView.setFastScrollEnabled(false);
        stickyArrAdapter = new StickyArrAdapter(this, list);

        scaleInAnimationAdapter = new ScaleInAnimationAdapter(stickyArrAdapter);
        stickyListHeadersAdapterDecorator = new StickyListHeadersAdapterDecorator(scaleInAnimationAdapter);
        stickyListHeadersAdapterDecorator.setStickyListHeadersListView(listView);
        listView.setAdapter(stickyListHeadersAdapterDecorator);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Item # " + String.valueOf(i)
                        + " " + adapterView.getAdapter().getItem(i), Toast.LENGTH_SHORT).show();
                listView.getTag();
                listView.smoothScrollToPosition(i + 5);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                mScrollState = scrollState;
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {

            }
        });

        new UpdaterAsyncTask().execute();

    }

    private void updateAdapterContent(int position, int value) {
        List<ListArrModel> listArrModel = stickyArrAdapter.getObjects();
        listArrModel.get(position).setSubText(String.valueOf(value));
        listArrModel.get(position).setDirty(false);
        stickyArrAdapter.notifyDataSetChanged();
        listView.invalidate();
    }

    private class UpdaterAsyncTask extends AsyncTask<Void, Void, Void> {
        int counter = 0;

        @Override
        protected Void doInBackground(Void... params) {

            while (counter != 100) {
                counter += 1;
                List<ListArrModel> listArrModel = stickyArrAdapter.getObjects();
                listArrModel.get(0).setSubText(String.valueOf(counter));
                listArrModel.get(0).setDirty(true);
                publishProgress();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... params) {
            super.onProgressUpdate();
            if (mScrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                int start = listView.getFirstVisiblePosition();
                for (int i = start, j = listView.getLastVisiblePosition(); i <= j; i++) {
                    //View view = listView.getChildAt(i - start);
                    if (((ListArrModel) listView.getItemAtPosition(0)).isDirty()) {
                        updateAdapterContent(0, counter);
                    }

                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sticky_headers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
