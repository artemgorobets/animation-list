package com.customertimes.animationlistactivity;



import com.customertimes.animationlistactivity.models.ListModel;

import java.util.List;

/**
 * Created by Artem Gorobets
 * 9/2/14.
 */
public class ListEvent {
    public List<ListModel> list;
}
