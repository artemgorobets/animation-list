package com.customertimes.animationlistactivity.models;

import com.customertimes.animationlistactivity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artem Gorobets
 * 9/9/14.
 */
public class ArrayGenerator {


    private List<ListArrModel> modelList;

    public List<ListArrModel> getArray() {
        modelList = new ArrayList<ListArrModel>();

        ListArrModel model = new ListArrModel();

        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");;
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);


        model = new ListArrModel();
        model.setMainText("Item A");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group A");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item B");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group B");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");;
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);

        model = new ListArrModel();
        model.setMainText("Item C");
        model.setSubText("0");
        model.setImageSrc(R.drawable.music);
        model.setGroup("Group C");
        modelList.add(model);


        return modelList;
    }
}
