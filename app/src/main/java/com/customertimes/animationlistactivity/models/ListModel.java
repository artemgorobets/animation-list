package com.customertimes.animationlistactivity.models;
/**
 * Created by Artem Gorobets
 * 9/2/14.
 */
public class ListModel {

    String name;
    String surname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}