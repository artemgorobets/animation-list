package com.customertimes.animationlistactivity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.customertimes.animationlistactivity.R;
import com.customertimes.animationlistactivity.models.ListModel;

import java.util.List;

/**
 * Created by Artem Gorobets
 * 9/2/14.
 */
public class ListAdapter extends BaseAdapter {

    private List<ListModel> list = null;
    private Context context;

    public ListAdapter(Context context, List<ListModel> list) {
        super();
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_item, null);
        LinearLayout container = (LinearLayout) view.findViewById(R.id.list_container);
        ListModel listModel = (ListModel) getItem(i);

        TextView fieldName = (TextView) view.findViewById(R.id.label_name);
        TextView fieldSurname = (TextView) view.findViewById(R.id.label_surname);

        fieldName.setText(listModel.getName());
        fieldSurname.setText(listModel.getSurname());

        /*Animation animationY = new TranslateAnimation(0, 0, viewGroup.getHeight()/4, 0);
        animationY.setDuration(300);
        view.startAnimation(animationY);
        animationY = null;*/


        return view;
    }
}