package com.customertimes.animationlistactivity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.customertimes.animationlistactivity.BusProvider;
import com.customertimes.animationlistactivity.R;
import com.customertimes.animationlistactivity.models.ListArrModel;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Artem Gorobets
 * 9/2/14.
 */
public class StickyArrAdapter extends BaseAdapter implements StickyListHeadersAdapter {
    private final Context mContext;
    private List<ListArrModel> list = null;

    LayoutInflater inflater;

    public StickyArrAdapter(Context context, List<ListArrModel> list) {
        this.list = list;
        mContext = context;
        inflater = LayoutInflater.from(context);
        BusProvider.getInstance().register(this);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ListArrModel listArrModel = (ListArrModel) getItem(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.list_row_text);
            holder.subtext = (TextView) convertView.findViewById(R.id.list_row_subtext);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText(listArrModel.getMainText());
        holder.subtext.setText(listArrModel.getSubText());
        holder.progressBar.setProgress(Integer.valueOf(listArrModel.getSubText()));
        holder.dirty = listArrModel.isDirty();
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.h_text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        String headerText = "" + list.get(position).getGroup();
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        long actualPosition = list.get(position)
                .getGroup().subSequence(0, list.get(position)
                        .getGroup().length()).hashCode();
        return actualPosition;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<ListArrModel> getObjects() {
        return list;
    }

    class ViewHolder {
        TextView text;
        TextView subtext;
        ProgressBar progressBar;
        boolean dirty;
    }

    class HeaderViewHolder {
        TextView text;
    }

}
