package com.customertimes.animationlistactivity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.customertimes.animationlistactivity.R;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Artem Gorobets
 * 9/2/14.
 */
public class StickyAdapter extends BaseAdapter implements StickyListHeadersAdapter {
    private final Context mContext;

    private String[] countries;
    LayoutInflater inflater;


    public StickyAdapter(Context context) {
        mContext = context;
        countries = context.getResources().getStringArray(R.array.countries);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.list_row_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText(countries[position]);
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.h_text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        String headerText = "" + countries[position].subSequence(0,1).charAt(0);
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        System.out.println(position);
        return countries[position].subSequence(0, 1).charAt(0);
    }

    @Override
    public int getCount() {
        return countries.length;
    }

    @Override
    public Object getItem(int i) {
        return countries[i];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        TextView text;
    }

    class HeaderViewHolder {
        TextView text;
    }
}
