package com.customertimes.animationlistactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.customertimes.animationlistactivity.adapters.ListAdapter;
import com.customertimes.animationlistactivity.models.ListModel;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.squareup.otto.Subscribe;

import java.util.List;


public class MainActivity extends Activity {

    ListView listView;
    List<ListModel> list;
    ListAdapter adapter;
    private AnimationAdapter mAnimAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listView = (ListView) findViewById(R.id.list_view);
        BusProvider.getInstance().register(this);
        //FakeUsersArray fakeUsersArray = new FakeUsersArray();
        //fakeUsersArray.getUsersArray();
        Intent intent = new Intent(getApplicationContext(), StickyHeaders.class);
        startActivity(intent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void handleListLoaded(ListEvent event) {
        System.out.println("received");
        List<ListModel> list = event.list;
        adapter = new ListAdapter(this, list);
        //setRightAdapter();
        //setBottomAdapter();
        setScaleAdapter();

    }

    private void setAlphaAdapter() {
        if (!(mAnimAdapter instanceof AlphaInAnimationAdapter)) {
            mAnimAdapter = new AlphaInAnimationAdapter(adapter);
            mAnimAdapter.setAbsListView(listView);
            listView.setAdapter(mAnimAdapter);
        }
    }

    private void setRightAdapter() {
        if (!(mAnimAdapter instanceof SwingRightInAnimationAdapter)) {
            mAnimAdapter = new SwingRightInAnimationAdapter(adapter);
            mAnimAdapter.setAbsListView(listView);
            listView.setAdapter(mAnimAdapter);
        }
    }

    private void setBottomAdapter() {
        if (!(mAnimAdapter instanceof SwingBottomInAnimationAdapter)) {
            mAnimAdapter = new SwingBottomInAnimationAdapter(adapter);
            mAnimAdapter.setAbsListView(listView);
            listView.setAdapter(mAnimAdapter);
        }
    }

    private void setScaleAdapter() {
        if (!(mAnimAdapter instanceof ScaleInAnimationAdapter)) {
            mAnimAdapter = new ScaleInAnimationAdapter(adapter);
            mAnimAdapter.setAbsListView(listView);
            listView.setAdapter(mAnimAdapter);
        }
    }
}
