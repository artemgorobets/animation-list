package com.customertimes.animationlistactivity;

import com.squareup.otto.Bus;

/**
 * Created by Artem Gorobets
 * 8/8/14.
 */
public final class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {}
}
